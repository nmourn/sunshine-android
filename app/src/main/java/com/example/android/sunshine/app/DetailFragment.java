package com.example.android.sunshine.app;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.sunshine.app.data.WeatherContract;

public class DetailFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String LOG_TAG = DetailFragment.class.getSimpleName();

    private static final String FORECAST_SHARE_HASHTAG = " #SunshineApp";
    private static final int DETAILS_LOADER_ID = 2;

    private static final String[] FORECAST_COLUMNS = {
            WeatherContract.WeatherEntry.TABLE_NAME + "." + WeatherContract.WeatherEntry._ID,
            WeatherContract.WeatherEntry.COLUMN_DATE,
            WeatherContract.WeatherEntry.COLUMN_SHORT_DESC,
            WeatherContract.WeatherEntry.COLUMN_MAX_TEMP,
            WeatherContract.WeatherEntry.COLUMN_MIN_TEMP,
            WeatherContract.WeatherEntry.COLUMN_HUMIDITY,
            WeatherContract.WeatherEntry.COLUMN_WIND_SPEED,
            WeatherContract.WeatherEntry.COLUMN_DEGREES,
            WeatherContract.WeatherEntry.COLUMN_PRESSURE,
            WeatherContract.WeatherEntry.COLUMN_WEATHER_ID
    };

    // These indices are tied to FORECAST_COLUMNS.  If FORECAST_COLUMNS changes, these
    // must change.
    static final int COL_WEATHER_ID = 0;
    static final int COL_WEATHER_DATE = 1;
    static final int COL_WEATHER_DESC = 2;
    static final int COL_WEATHER_MAX_TEMP = 3;
    static final int COL_WEATHER_MIN_TEMP = 4;
    static final int COL_WEATHER_HUMIDITY = 5;
    static final int COL_WEATHER_WIND_SPEED = 6;
    static final int COL_WEATHER_WIND_ANGLE = 7;
    static final int COL_WEATHER_PRESSURE = 8;
    static final int COL_WEATHER_COND_ID = 9;

    private static final String DETAIL_URI_ARG = "uri_arg";

    private String mForecast;
    private Uri mUri;
    private TextView mDescriptionView, mMinTempView, mMaxTempView, mHumidityView, mWindView,
        mPressureView, mDayNameView, mDateView;
    private ImageView mWeatherIcon;
    private ShareActionProvider mShareActionProvider;

    public static DetailFragment makeFragment(Uri detailUri) {
        Bundle args = new Bundle();
        args.putParcelable(DETAIL_URI_ARG, detailUri);
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public DetailFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (null != mUri) {
            return new CursorLoader(getActivity(), mUri,
                    FORECAST_COLUMNS, null, null, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        populateView(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        getLoaderManager().initLoader(DETAILS_LOADER_ID, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle args = getArguments();
        if (args != null) {
            mUri = args.getParcelable(DETAIL_URI_ARG);
        }

        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        mWeatherIcon = (ImageView) rootView.findViewById(R.id.details_icon);
        mDayNameView = (TextView) rootView.findViewById(R.id.details_day_name_text);
        mDateView = (TextView) rootView.findViewById(R.id.details_day_calendar_text);
        mMaxTempView = (TextView) rootView.findViewById(R.id.details_temp_high);
        mMinTempView = (TextView) rootView.findViewById(R.id.details_temp_low);
        mDescriptionView = (TextView) rootView.findViewById(R.id.details_weather_description);
        mHumidityView = (TextView) rootView.findViewById(R.id.details_humidity);
        mWindView = (TextView) rootView.findViewById(R.id.details_wind);
        mPressureView = (TextView) rootView.findViewById(R.id.details_pressure);
        // The detail Activity called via intent.  Inspect the intent for forecast data.

        return rootView;
    }

    public void onLocationChanged(String newLocation) {
        if (mUri != null) {
            long date = WeatherContract.WeatherEntry.getDateFromUri(mUri);
            mUri = WeatherContract.WeatherEntry.buildWeatherLocationWithDate(newLocation, date);
            getLoaderManager().restartLoader(DETAILS_LOADER_ID, null, this);
        }
    }

    private void populateView(Cursor c) {
        if (c == null || !c.moveToFirst()) {
            return;
        }
        long date = c.getLong(COL_WEATHER_DATE);
        String dayString = Utility.getDayName(getActivity(), date);
        String dateString = Utility.getFormattedMonthDay(getActivity(), date);
        String weatherDesc = c.getString(COL_WEATHER_DESC);

        String high = Utility.formatTemperature(getActivity(),
                c.getDouble(COL_WEATHER_MAX_TEMP));
        String low = Utility.formatTemperature(getActivity(),
                c.getDouble(COL_WEATHER_MIN_TEMP));

        String wind = Utility.getFormattedWind(getActivity(),
                c.getDouble(COL_WEATHER_WIND_SPEED), c.getDouble(COL_WEATHER_WIND_ANGLE));
        String humidity = getActivity().getString(R.string.format_humidity,
                c.getDouble(COL_WEATHER_HUMIDITY));
        String pressure = getActivity().getString(R.string.format_pressure,
                c.getDouble(COL_WEATHER_PRESSURE));
        int weatherCondId = c.getInt(COL_WEATHER_COND_ID);


        mDayNameView.setText(dayString);
        mDateView.setText(dateString);
        mDescriptionView.setText(weatherDesc);
        mMaxTempView.setText(high);
        mMinTempView.setText(low);
        mHumidityView.setText(humidity);
        mWindView.setText(wind);
        mPressureView.setText(pressure);
        mWeatherIcon.setImageResource(Utility.getWeatherConditionArtResource(weatherCondId));

        mForecast = String.format("%s - %s - %s/%s", dateString, weatherDesc, high, low);
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(createShareForecastIntent());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.detailfragment, menu);

        // Retrieve the share menu item
        MenuItem menuItem = menu.findItem(R.id.action_share);

        // Get the provider and hold onto it to set/change the share intent.
        mShareActionProvider =
                (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);

        // Attach an intent to this ShareActionProvider.  You can update this at any time,
        // like when the user selects a new piece of data they might like to share.
        if (mForecast != null ) {
            mShareActionProvider.setShareIntent(createShareForecastIntent());
        } else {
            Log.d(LOG_TAG, "Share Action Provider is null?");
        }
    }

    private Intent createShareForecastIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,
                mForecast + FORECAST_SHARE_HASHTAG);
        return shareIntent;
    }
}